# Given an array, find the average of all contiguous subarrays of size ‘K’ in it.
# Input (array, sub-array size K): [1, 3, 2, 6, -1, 4, 1, 8, 2], K=5
# Output (array of averages of sub-array): [2.2, 2.8, 2.4, 3.6, 2.8]
# brute force algo. time complexity will be quadratic O(n^2). 
# to reduce time complexity: we could reuse sum calculated for earlier sub-array in next sub-array. 
# subtracting previous element(s) and adding new element(s). most re-usable sub-array will be the immediate previous as it will require just 1 addition and 1 subtraction
# this looks like sliding-window and hence the pattern name

# 1,2,3,4,5,6,7,8,9
# [ Sub1  ] 6 7 8 9    Sub1 = 1+2+3+4+5
# 1 [ Sub2  ] 7 8 9    Sub2 = -1+Sub1+6, 2+3+4+5+6
# 1 2 [ Sub3  ] 8 9    Sub3 = -2+Sub2+7, -1-2+Sub1+6+7, 
# 1 2 3 [ Sub4  ] 9    Sub4 = -3+Sub3+8, -2-3+Sub2+7+8, -1-2-3+Sub1+6+7+8
# 1 2 3 4 [ Sub5  ]    Sub5 = -4+Sub4+9, ...


def findAllAverages(inputArray, K):
    if len(inputArray) < K: 
        raise ValueError("array size should be greater than K")

    sub = sum(inputArray[:K])
    averages = [sub/K]
    for i in range(1,len(inputArray)-K+1):
        nextSub = sub - inputArray[i-1] + inputArray[i+K-1]
        averages.append(nextSub/K)
        sub = nextSub
    return averages

# test 
if __name__ == "__main__": 
    assert findAllAverages([1, 3, 2, 6, -1, 4, 1, 8, 2],5) == [2.2, 2.8, 2.4, 3.6, 2.8]
    try:
        findAllAverages([1, 3, 2, 6, -1, 4, 1, 8, 2], 15)
        assert False, 'failed check: window size > array size'
    except ValueError:
        assert True

